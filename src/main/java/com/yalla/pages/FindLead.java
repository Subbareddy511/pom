package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLead extends Annotations {
public FindLead() {
	PageFactory.initElements(driver, this);
}

/*@FindBy(how=How.XPATH,using="//span[text()='Name and ID']") WebElement eleNameandId;

public   FindLead clickNameAndId() {
	click(eleNameandId);
	return this;
}*/
@FindBy(how=How.XPATH,using="//input[@name='id']") WebElement eleEnterleadid;

public   FindLead enterLeadId(String data) {
	clearAndType(eleEnterleadid ,data);
	return this;
}
/*@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
public   FindLead enterFirstName(String data) {
	clearAndType(eleFirstName, data);
	return this;
	
}*/
@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLeadButton;
public   FindLead clickFindLeadButton() throws InterruptedException {
	click(eleFindLeadButton);
	Thread.sleep(2000);
	return this;
	
}
@FindBy(how=How.XPATH,using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a") WebElement eleleadidvalue;
public  ViewLead clickLeadidvalue() {
	
 /*String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
	click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));*/
	click(eleleadidvalue);
	
	return new ViewLead();
}



//enter the lead id
@FindBy(how=How.XPATH,using="//input[@name='id']") WebElement eleLeadId;
public   FindLead searchLeadId(String text) {
	clearAndType(eleLeadId, text);
	return this;
}
public FindLead findleadResult() {
	verifyPartialText(locateElement("xpath","//div[@class='x-paging-info']"), "No records to display");
	return this;
}
}